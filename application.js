const dateEl = document.querySelector("#date");
const useridEl = document.querySelector("#id");
const usernameEl = document.querySelector("#username");
const designationEl = document.querySelector("#designation");
const reasonEl = document.querySelector("#reason");
const startDateEl = document.querySelector("#start");
const endDateEl = document.querySelector("#end");
const form = document.querySelector("#application");

form.addEventListener("submit", function (e) {
  e.preventDefault();

  let isDateValid = checkDate(),
    isUseridValid = checkUserid(),
    isUsernameValid = checkUsername(),
    isDesignationVallid = checkDesignation(),
    isReasonValid = checkReason(),
    isStartDateValid = checkStartDate(),
    isEndDateValid = checkEndDate();
  let isFormValid =
    isDateValid &&
    isUseridValid &&
    isUsernameValid &&
    isDesignationVallid &&
    isReasonValid &&
    isStartDateValid &&
    isEndDateValid;
  if (isFormValid) {
  }
});

function checkDate() {
  let valid = false;

  const date = dateEl.value.trim();
  if (!isRequired(date)) {
    showError(dateEl, "Date cannot be blank.");
  } else {
    showSuccess(dateEl);
    valid = true;
  }
  return valid;
}

const isUseridValid = (userid) => {
  const re = /^([0-9])+$/;
  return re.test(userid);
};

function checkUserid() {
  let valid = false;
  const userid = useridEl.value.trim();
  if (!isRequired(userid)) {
    showError(useridEl, "User ID cannot be blank.");
  } else {
    showSuccess(useridEl);
    valid = true;
  }
  return valid;
}

function checkUsername() {
  let valid = false;
  const username = usernameEl.value;
  if (!isRequired(username)) {
    showError(usernameEl, "Username cannot be blank.");
  } else {
    showSuccess(usernameEl);
    valid = true;
  }
  return valid;
}

let isRequired = (value) => (value === "" ? false : true);

let showError = (input, message) => {
  let formField = input.parentElement;
  formField.classList.remove("success");
  formField.classList.add("error");

  const error = formField.querySelector("small");
  error.textContent = message;
};

const showSuccess = (input) => {
  const formField = input.parentElement;
  formField.classList.remove("error");
  formField.classList.add("success");

  formField.querySelector("small").textContent = "";
};

function checkReason() {
  let valid = false;
  const Reason = reasonEl.value.trim();
  if (!isRequired(Reason)) {
    showError(reasonEl, "You have to set a Reason");
  } else {
    showSuccess(reasonEl);
    valid = true;
  }
  return true;
}

function checkStartDate() {
  let valid = false;
  const endDate = startDateEl.value.trim();
  if (!isRequired(startDate)) {
    showError(startDateEl, "You will have to confirm to proceed");
  } else {
    showSuccess(startDateEl);
    valid = true;
  }
  return valid;
}

function checkEndDate() {
  let valid = false;
  const endDate = endDateEl.value.trim();
  if (!isRequired(endDate)) {
    showError(endDateEl, "You will have to confirm to proceed");
  } else {
    showSuccess(endDateEl);
    valid = true;
  }
  return valid;
}
