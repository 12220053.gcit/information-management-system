const usernameEl = document.querySelector("#username");
const useridEl = document.querySelector("#userid");
const emailEl = document.querySelector("#email");
const passwordEl = document.querySelector("#password");
const confirmPasswordEl = document.querySelector("#confirm-password");
const form = document.querySelector("#signup");

form.addEventListener("submit", function (e) {
  e.preventDefault();

  let isUsernameValid = checkUsername(),
    isUseridValid = checkUserid(),
    isEmailValid = checkEmail(),
    isPasswordValid = checkPassword(),
    isConfirmPasswordValid = checkConfirmPassword();
  let isFormValid =
    isUsernameValid &&
    isUseridValid &&
    isEmailValid &&
    isPasswordValid &&
    isConfirmPasswordValid;
  if (isFormValid) {
  }
});

function checkUsername() {
  let valid = false;
  const min = 3,
    max = 25;
  const username = usernameEl.value.trim();
  if (!isRequired(username)) {
    showError(usernameEl, "Username cannot be blank.");
  } else if (!isBetween(username.length, min, max)) {
    showError(
      usernameEl,
      `Username must be between ${min} and ${max} characters.`
    );
  } else {
    showSuccess(usernameEl);
    valid = true;
  }
  return valid;
}

const isUsernameValid = (username) => {
  const re = /^([0-9])+$/;
  return re.test(username);
};

function checkUserid() {
  let valid = false;
  const min = 3,
    max = 25;
  const userid = useridEl.value.trim();
  if (!isRequired(userid)) {
    showError(useridEl, "Username cannot be blank.");
  } else if (!isUsernameValid(userid)) {
    showError(useridEl, "Invalid username. Must contai only the digits");
  } else if (!isBetween(userid.length, min, max)) {
    showError(
      useridEl,
      `Username must be between ${min} and ${max} characters.`
    );
  } else {
    showSuccess(useridEl);
    valid = true;
  }
  return valid;
}

let isRequired = (value) => (value === "" ? false : true);

const isBetween = (length, min, max) =>
  length < min || length > max ? false : true;

let showError = (input, message) => {
  let formField = input.parentElement;
  formField.classList.remove("success");
  formField.classList.add("error");

  const error = formField.querySelector("small");
  error.textContent = message;
};

const showSuccess = (input) => {
  const formField = input.parentElement;
  formField.classList.remove("error");
  formField.classList.add("success");

  formField.querySelector("small").textContent = "";
};

const isEmailValid = (email) => {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

function checkEmail() {
  let valid = false;
  const email = emailEl.value.trim();
  if (!isRequired(email)) {
    showError(emailEl, " Email cannot be blank.");
  } else if (!isEmailValid(email)) {
    showError(emailEl, `Email is not valid. Should be like example@gmail.com`);
  } else {
    showSuccess(emailEl);
    valid = true;
  }
  return valid;
}
const isPasswordValid = (password) => {
  const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
  return re.test(password);
};

function checkPassword() {
  let valid = false;
  const min = 3,
    max = 25;
  const password = passwordEl.value.trim();
  if (!isRequired(password)) {
    showError(passwordEl, "You have to set a password");
  } else if (!isPasswordValid(password)) {
    showError(
      passwordEl,
      "Password must be at least 8 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number and 1 special character "
    );
  } else {
    showSuccess(passwordEl);
    valid = true;
  }
  return true;
}

function checkConfirmPassword() {
  let valid = false;
  const passwordCheck = confirmPasswordEl.value.trim();
  if (!isRequired(passwordCheck)) {
    showError(confirmPasswordEl, "You will have to confirm to proceed");
  } else if (!passwordMatch()) {
    showError(confirmPasswordEl, "Password is not matching");
  } else {
    showSuccess(confirmPasswordEl);
    valid = true;
  }
  return valid;
}
const passwordMatch = () => {
  if (passwordEl.value == confirmPasswordEl.value) {
    return true;
  }
};
